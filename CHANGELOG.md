## [1.0.6](https://gitlab.com/mfgames-writing/mfgames-writing-liquid-js/compare/v1.0.5...v1.0.6) (2018-08-09)


### Bug Fixes

* fixing tsconfig.json build generation ([ff6ac12](https://gitlab.com/mfgames-writing/mfgames-writing-liquid-js/commit/ff6ac12))

## [1.0.5](https://gitlab.com/mfgames-writing/mfgames-writing-liquid-js/compare/v1.0.4...v1.0.5) (2018-08-09)


### Bug Fixes

* adding missing publish npm task to semantic-release ([d52f56f](https://gitlab.com/mfgames-writing/mfgames-writing-liquid-js/commit/d52f56f))

## [1.0.4](https://gitlab.com/mfgames-writing/mfgames-writing-liquid-js/compare/v1.0.3...v1.0.4) (2018-08-09)


### Bug Fixes

* trying to get ci to build cleanly ([ef34e69](https://gitlab.com/mfgames-writing/mfgames-writing-liquid-js/commit/ef34e69))

## [1.0.3](https://gitlab.com/mfgames-writing/mfgames-writing-liquid-js/compare/v1.0.2...v1.0.3) (2018-08-09)


### Bug Fixes

* fixing package references ([d1736a0](https://gitlab.com/mfgames-writing/mfgames-writing-liquid-js/commit/d1736a0))

## [1.0.2](https://gitlab.com/mfgames-writing/mfgames-writing-liquid-js/compare/v1.0.1...v1.0.2) (2018-08-09)


### Bug Fixes

* fixing bad package.json entry points ([a5d4126](https://gitlab.com/mfgames-writing/mfgames-writing-liquid-js/commit/a5d4126))
