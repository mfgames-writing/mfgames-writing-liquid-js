# Liquid theme support for MfGames Writing (Javascript).

This is a support library for implementing a Liquid-based theme inspired by Jekyll's theme processing. This also uses SASS for CSS generation.
